package com.yanisallouch.inscription;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class PlanningViewModel extends ViewModel {

    private MutableLiveData<String> liveMatin_8h;
    private MutableLiveData<String> liveMatin_10h;
    private MutableLiveData<String> liveSoir_14h;
    private MutableLiveData<String> liveSoir_16h;

    public MutableLiveData<String> getLiveMatin_8h() {
        if (liveMatin_8h == null) {
            liveMatin_8h = new MutableLiveData<String>();
        }
        return liveMatin_8h;
    }

    public void setLiveMatin_8h(String str){
        liveMatin_8h.setValue(str);
    }

    public MutableLiveData<String> getLiveMatin_10h() {
        if (liveMatin_10h == null) {
            liveMatin_10h = new MutableLiveData<String>();
        }
        return liveMatin_10h;
    }

    public void setLiveMatin_10h(String str){
        liveMatin_10h.setValue(str);
    }

    public MutableLiveData<String> getLiveSoir_14h() {
        if (liveSoir_14h == null) {
            liveSoir_14h = new MutableLiveData<String>();
        }
        return liveSoir_14h;
    }

    public void setLiveSoir_14h(String str){
        liveSoir_14h.setValue(str);
    }

    public MutableLiveData<String> getLiveSoir_16h() {
        if (liveSoir_16h == null) {
            liveSoir_16h = new MutableLiveData<String>();
        }
        return liveSoir_16h;
    }

    public void setLiveSoir_16h(String str){
        liveSoir_16h.setValue(str);
    }

}
