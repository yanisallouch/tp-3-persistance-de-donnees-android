package com.yanisallouch.inscription;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.room.Room;

import java.util.Random;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class Planning extends AppCompatActivity {

    private PlanningViewModel modelMatin8h;
    private PlanningViewModel modelMatin10h;
    private PlanningViewModel modelSoir14h;
    private PlanningViewModel modelSoir16h;
    private TextView textView;

    PlanningDao planningDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_planning);

        StringBuilder data = new StringBuilder();
        data.append("Rencontre client Dupont").append("\n");
        data.append("Travailler le dossier recrutement").append("\n");
        data.append("Réunion équipe").append("\n");
        data.append("Préparation dossier vente").append("\n");

        AppDatabase db = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, "planning").allowMainThreadQueries().build();
        PlanningDao planningDao = db.planningDao();

        planningDao.insert(new PlanningBDD(0,"08h-10h", "Rencontre client Dupont"));
        planningDao.insert(new PlanningBDD(1,"10h-12h", "Travailler le dossier recrutement"));
        planningDao.insert(new PlanningBDD(2,"14h-16h", "Réunion équipe"));
        planningDao.insert(new PlanningBDD(3,"16h-18h", "Préparation dossier vente"));

        // Get the ViewModel.
        modelMatin8h = new ViewModelProvider(this).get(PlanningViewModel.class);
        modelMatin10h = new ViewModelProvider(this).get(PlanningViewModel.class);
        modelSoir14h = new ViewModelProvider(this).get(PlanningViewModel.class);
        modelSoir16h = new ViewModelProvider(this).get(PlanningViewModel.class);

        // Create the observer which updates the UI.
        final Observer<String> matin8hObserver = new Observer<String>() {
            @Override
            public void onChanged(@Nullable final String newName) {
                // Update the UI, in this case, a TextView.
                textView = findViewById(R.id.matin_8h);
                textView.setText(newName);
            }
        };
        // Observe the LiveData, passing in this activity as the LifecycleOwner and the observer.
        modelMatin8h.getLiveMatin_8h().observe(this, matin8hObserver);
        modelMatin8h.setLiveMatin_8h(planningDao.findByPeriode("08h-10h").contenu);

        // Create the observer which updates the UI.
        final Observer<String> matin10hObserver = new Observer<String>() {
            @Override
            public void onChanged(@Nullable final String newName) {
                // Update the UI, in this case, a TextView.
                textView = findViewById(R.id.matin_10h);
                textView.setText(newName);
            }
        };
        // Observe the LiveData, passing in this activity as the LifecycleOwner and the observer.
        modelMatin10h.getLiveMatin_10h().observe(this, matin10hObserver);
        modelMatin10h.setLiveMatin_10h(planningDao.findByPeriode("10h-12h").contenu);

        // Create the observer which updates the UI.
        final Observer<String> soir14hObserver = new Observer<String>() {
            @Override
            public void onChanged(@Nullable final String newName) {
                // Update the UI, in this case, a TextView.
                textView = findViewById(R.id.soir_14h);
                textView.setText(newName);
            }
        };
        // Observe the LiveData, passing in this activity as the LifecycleOwner and the observer.
        modelSoir14h.getLiveSoir_14h().observe(this, soir14hObserver);
        modelSoir14h.setLiveSoir_14h(planningDao.findByPeriode("14h-16h").contenu);

        // Create the observer which updates the UI.
        final Observer<String> soir16hObserver = new Observer<String>() {
            @Override
            public void onChanged(@Nullable final String newName) {
                // Update the UI, in this case, a TextView.
                textView = findViewById(R.id.soir_16h);
                textView.setText(newName);
            }
        };
        // Observe the LiveData, passing in this activity as the LifecycleOwner and the observer.
        modelSoir16h.getLiveSoir_16h().observe(this, soir16hObserver);
        modelSoir16h.setLiveSoir_16h(planningDao.findByPeriode("16h-18h").contenu);

        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                modelMatin8h.setLiveMatin_8h(planningDao.findByPeriode("08h-10h").contenu);
                modelMatin10h.setLiveMatin_10h(planningDao.findByPeriode("10h-12h").contenu);
                modelSoir14h.setLiveSoir_14h(planningDao.findByPeriode("14h-16h").contenu);
                modelSoir16h.setLiveSoir_16h(planningDao.findByPeriode("16h-18h").contenu);

                handler.postDelayed(this, 1000);
            }
        });
    }
}