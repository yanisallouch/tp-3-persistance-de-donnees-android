package com.yanisallouch.inscription;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    FileOutputStream outputStream;
    TextView textView;
    EditText editableText;
    private final Random random = new Random();
    private final static String KEY_FIRST_NAME = "first_name";
    private final static String KEY_LAST_NAME = "last_name";
    private final static String KEY_AGE = "age";
    private final static String KEY_PHONE_NUMBER = "phone_number";
    private final static String KEY_RANDOM_ID = "random_id";

    private String firstName = "";
    private String lastName = "";
    private String age = "";
    private String phoneNumber = "";
    private String randomID = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState != null) {
            firstName = savedInstanceState.getString(KEY_FIRST_NAME);
            lastName = savedInstanceState.getString(KEY_LAST_NAME);
            age = savedInstanceState.getString(KEY_AGE);
            phoneNumber = savedInstanceState.getString(KEY_PHONE_NUMBER);
            randomID = savedInstanceState.getString(KEY_RANDOM_ID);
            editableText = findViewById(R.id.editTextFirstName);
            editableText.setText(firstName);
            editableText = findViewById(R.id.editTextLastName);
            editableText.setText(lastName);
            editableText = findViewById(R.id.editTextAge);
            editableText.setText(age);
            editableText = findViewById(R.id.editTextPhoneNumber);
            editableText.setText(phoneNumber);
        }else {
            generateRandomID();
            textView = findViewById(R.id.textRandomID);
            textView.setText(randomID);

        }
    }

    private void generateRandomID() {
        randomID = String.valueOf(random.nextDouble());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            firstName = savedInstanceState.getString(KEY_FIRST_NAME);
            lastName = savedInstanceState.getString(KEY_LAST_NAME);
            age = savedInstanceState.getString(KEY_AGE);
            phoneNumber = savedInstanceState.getString(KEY_PHONE_NUMBER);
            randomID = savedInstanceState.getString(KEY_RANDOM_ID);
            editableText = findViewById(R.id.editTextFirstName);
            editableText.setText(firstName);
            editableText = findViewById(R.id.editTextLastName);
            editableText.setText(lastName);
            editableText = findViewById(R.id.editTextAge);
            editableText.setText(age);
            editableText = findViewById(R.id.editTextPhoneNumber);
            editableText.setText(phoneNumber);
            textView = findViewById(R.id.textRandomID);
            textView.setText(randomID);
            Toast.makeText(this, "Etat de l'activité restauré",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

        editableText = findViewById(R.id.editTextFirstName);
        firstName = editableText.getText().toString();
        savedInstanceState.putString(KEY_FIRST_NAME, firstName);

        editableText = findViewById(R.id.editTextLastName);
        lastName = editableText.getText().toString();
        savedInstanceState.putString(KEY_LAST_NAME, lastName);

        editableText = findViewById(R.id.editTextAge);
        age = editableText.getText().toString();
        savedInstanceState.putString(KEY_AGE, age);

        editableText = findViewById(R.id.editTextPhoneNumber);
        phoneNumber = editableText.getText().toString();
        savedInstanceState.putString(KEY_PHONE_NUMBER, phoneNumber);

        textView = findViewById(R.id.textRandomID);
        randomID = textView.getText().toString();
        savedInstanceState.putString(KEY_RANDOM_ID, randomID);

        Toast.makeText(this, "Etat de l'activité sauvegardé",
                Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        getLifecycle().removeObserver(new MainActivityObserver());
    }

    @Override
    protected void onResume() {
        super.onResume();
        getLifecycle().addObserver(new MainActivityObserver());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "L'activité est détruite", Toast.LENGTH_SHORT)
                .show();

    }


    public void onSubmit(View view) {
        editableText = findViewById(R.id.editTextFirstName);
        firstName = editableText.getText().toString();

        String filename = firstName.toLowerCase() + "_" + randomID;

        editableText = findViewById(R.id.editTextLastName);
        lastName = editableText.getText().toString();

        editableText = findViewById(R.id.editTextAge);
        age = editableText.getText().toString();

        editableText = findViewById(R.id.editTextPhoneNumber);
        phoneNumber = editableText.getText().toString();

        textView = findViewById(R.id.textRandomID);
        randomID = textView.getText().toString();


        try {
            outputStream = openFileOutput(filename, Context.MODE_PRIVATE);
            String concatenation = getString(R.string.first_name) + ": " + firstName + "\n" +
                    getString(R.string.last_name) + ": " + lastName + "\n" +
                    getString(R.string.age) + ": " + age + "\n" +
                    getString(R.string.phone_number) + ": " + phoneNumber + "\n" +
                    getString(R.string.random_id) + ": " + randomID + "\n" +
                    "Nombre d'utilisation: " + MainActivityObserver.compteur ;
            outputStream.write(concatenation.getBytes());
        } catch (FileNotFoundException e) {
            Log.e("Inscription", "Fichier: <"+ filename + "> raised: " + e.getCause());
            Log.e("Inscription", "File StackTrace: " + Arrays.toString(e.getStackTrace()));
        } catch (IOException e) {
            Log.e("Inscription", "IO StackTrace: " + Arrays.toString(e.getStackTrace()));
        }finally {
            try {
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        Intent intent=new Intent(this, MainActivity2.class);
        intent.putExtra(MainActivity2.INTENT_FILENAME, filename);
        startActivity(intent);
    }

    public void onSubmitPlanning(View view) {
        Intent intent=new Intent(this, Planning.class);
        startActivity(intent);
    }

    private static class MainActivityObserver implements LifecycleObserver {
        private String TAG = this.getClass().getSimpleName();
        public static int compteur = 0;

        @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
        private static void NombreUtilisation() {
            compteur++;
        }
    }
}