package com.yanisallouch.inscription;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {PlanningBDD.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract PlanningDao planningDao();
}
